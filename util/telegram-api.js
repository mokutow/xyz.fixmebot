'use strict';

/**
 * Module dependencies.
 */

const Promise = require('bluebird');
const request = require('request');

/**
 * Constants.
 */

const TELEGRAM_ENDPOINT = 'https://api.telegram.org/bot';

/**
 * TelegramApi class.
 */

class TelegramApi {

    static buildUrl(method) {
        return TELEGRAM_ENDPOINT + process.env.TELEGRAM_BOT_TOKEN + '/' + method;
    }

	static getChatAdministrators(chatId) {
		return new Promise((resolve, reject) => {
            const url = TelegramApi.buildUrl('getChatAdministrators');
			request({
                method: 'POST',
                url: url,
                json: {
                    chat_id: chatId
                }
            }, (err, res, body) => {
                if (err) {
                	reject(err);
                } else {
                	resolve(body.result);
                }
            });
		});
	}

};

module.exports = TelegramApi;