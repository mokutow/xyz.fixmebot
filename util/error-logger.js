'use strict';

/**
 * Module dependencies.
 */

const fs = require('fs');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const Error = mongoose.model('Error');

/**
 * ErrorLogger class.
 */

class ErrorLogger {
	constructor(errObject) {
		this.errObject = errObject;
	}

	_logToFile(err, json) {
		const self = this;
		return new Promise((resolve, reject) => {
			const logsDir = process.env.LOGS_DIR;
			const filePath = logsDir + '/err_' + json.timestamp + '.txt'; 
			fs.writeFile(filePath, JSON.stringify(json), writeErr => {
  				if (writeErr) {
  					reject(writeErr);
  				} else {
  					resolve();
  				}
			});
		});
	}

	log() {
		const self = this;
		return new Promise((resolve, reject) => {
			const errDetails = {
				message: self.errObject.message,
				stack: self.errObject.stack,
				timestamp: Date.now()
			};
			Error.create(errDetails, (mongoErr, record) => {
				if (mongoErr) {
					_logToFile(mongoErr, errDetails)
					.then(resolve);
				} else {
					resolve();
				}
			});
		});
	}
};

module.exports = ErrorLogger;