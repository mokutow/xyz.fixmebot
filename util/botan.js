'use strict';

/**
 * Module dependencies.
 */

const Promise = require('bluebird');
const request = require('request');

/**
 * Constants.
 */

const BOTAN_ENDPOINT = 'https://api.botan.io';

/**
 * BotanMetrics class.
 */

class BotanMetrics {

	static track(id, command, message) {
		return new Promise((resolve, reject) => {
			request({
                method: 'POST',
                url: BOTAN_ENDPOINT + '/track',
                qs: {
                    //token: process.env.BOTAN_TOKEN,
                    token: process.env.APPMETRICA_API_KEY,
                    uid: id,
                    name: command
                },
                json: message
            }, (err, res, body) => {
                if (err) {
                	reject(err);
                } else {
                	resolve();
                }
            });
		});
	}

};

module.exports = BotanMetrics;