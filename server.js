'use strict';

const path = require('path');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const requireDir = require('require-dir');
const TelegramBot = require('telegram-bot-api');
const BotanMetrics = require('./util/botan');

dotenv.load({
	path: path.join(__dirname, '.env.test')
});

let sendAnalyticsToBotan = (metricsData, message) => {
	return new Promise((resolve, reject) => {
		const chatId = metricsData[0];
		const command = metricsData[1];
		console.log('###chatId: ' + chatId + ', command: ' + command);

		if (!command) {
			resolve();
		} else {
			BotanMetrics.track(chatId, command, message)
			.then(resolve)
			.catch(reject);
		}
	});
};

const db = mongoose.connect(process.env.MONGO_URL, err => {
	if (err) {
		throw err;
	}

	autoIncrement.initialize(db);
	requireDir('./models');
	const ErrorLogger = require('./util/error-logger');

	let logError = (err) => {
		const logger = new ErrorLogger(err);
		logger.log()
		.then(() => {
			console.log('Error logged:');
			console.log(err);
		});
	}

	const bot = new TelegramBot({
		token: process.env.TELEGRAM_BOT_TOKEN,
    	updates: {
    		enabled: true
    	}
	});

	const CommandRouter = require('./command-router');
	const router = new CommandRouter(bot);
	bot.on('message', message => {
		router.routeMessage(message)
		.then(metricsData => {
			return sendAnalyticsToBotan(metricsData, message);
		})
		.then(() => {
			console.log('Route [message]: finished');
		}) 
		.catch(logError);
	});

	bot.on('inline.callback.query', query => {
		router.routeInlineCallbackQuery(query)
		.then(metricsData => {
			return sendAnalyticsToBotan(metricsData, query);
		})
		.then(() => {
			console.log('Route [inline.callback.query]: finished');
		}) 
		.catch(logError);
	});

	bot.on('inline.query', query => {
		router.routeInlineQuery(query)
		.then(metricsData => {
			return sendAnalyticsToBotan(metricsData, query);
		})
		.then(() => {
			console.log('Route [inline.query]: finished');
		}) 
		.catch(logError);
	});
});