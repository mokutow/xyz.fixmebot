'use strict';

/**
 * Module dependencies.
 */

const Promise = require('bluebird');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const BaseCommand = require('./base.js');

/**
 * TextCommand class.
 */

class TextCommand extends BaseCommand {

	_setTextToProcess() {
		const findQuery = { 
			chatId: this.getChatId() 
		};
		const setQuery = { 
			$set: { 
				textToProcess: this.message.text 
			}
		};
		return User.update(findQuery, setQuery).exec();
	}

	exec() {
		const self = this;
		return new Promise((resolve, reject) => {
			if (self.isGroupMessage()) {
				resolve(); //For now nothing for group messages
			} else {
				self.setCommandDesc('/co:::/re:::pending:::text_recieved');
				self._setTextToProcess()
				.then(() => {
					return self.getBotLocale();
				})
				.then(locale => {
					const buttons = [
						[
							{
								text: locale.nonCommand.buttonsText.correct,
								callback_data: '/co'
							}
						],
						[	
							{
								text: locale.nonCommand.buttonsText.rephrase,
								callback_data: '/re'
							}
						]
					];
					const messageText = locale.nonCommand.messageText;
					return self.sendMessageWithInlineButtons(false, messageText, buttons);
				})
				.then(resolve)
				.catch(reject);
			}
		});
	}
};

module.exports = TextCommand;