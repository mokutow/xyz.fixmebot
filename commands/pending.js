'use strict';

/**
 * Module dependencies.
 */

const Promise = require('bluebird');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const BaseCommand = require('./base.js');

/**
 * PendingCommand class.
 */

class PendingCommand extends BaseCommand {

	_updatePendingCommand(command) {
		const findQuery = { 
			chatId: this.getChatId() 
		};
		const setQuery = { 
			$set: { 
				pendingCommand: command 
			}
		};
		return User.update(findQuery, setQuery).exec();
	}

	_getPendingText() {
		const self = this;
		return new Promise((resolve, reject) => {
			const query = {
	        	chatId: self.getChatId()
	      	};
			User.findOne(query)
			.exec()
			.then(user => {
				resolve(user.textToProcess);
			})
			.catch(reject);
		});
	}

};

module.exports = PendingCommand;