'use strict';

/**
 * Module dependencies.
 */

const Promise = require('bluebird');
const BaseCommand = require('./base');
const mongoose = require('mongoose');
const Settings = mongoose.model('Settings');

/**
 * SettingsCommand class.
 */

class SettingsCommand extends BaseCommand {

	static canExec(message) {
		if ('text' in message && message.text.toLowerCase().startsWith('/settings')) {
			return true;
		}

		return false;
	}

	static getMainMenuCallbackData() {
		return [
			'show_highlight_menu',
			'show_lang_menu'
		];
	}

	static getHighlightMenuCallbackData() {
		return [
			'true',
			'false',
			'back_from_h'
		];
	}

	static getLangMenuCallbackData() {
		return [
			'en',
			'ru',
			'back_from_l'
		];
	}

	_getMainMenuButtons(locale) {
		const buttons = [];
		const buttonsText = locale.settings.keyboard.mainMenu;
		const callbackData = SettingsCommand.getMainMenuCallbackData();
		buttonsText.forEach((buttonText, index) => {
			buttons.push([{
				text: buttonText,
				callback_data: callbackData[index]
			}]);
		});
		return buttons;
	}

	_getHighlightMenuButtons(locale) {
		const buttons = [];
		const buttonsRow = [];
		const buttonsText = locale.settings.keyboard.highlightMenu;
		const callbackData = SettingsCommand.getHighlightMenuCallbackData();

		for (let i = 0; i < 2; i++) {
			buttonsRow.push({
				text: buttonsText[i],
				callback_data: callbackData[i]
			});
		}

		buttons.push(buttonsRow);
		buttons.push([{
			text: buttonsText[2],
			callback_data: callbackData[2]
		}]);
		return buttons;
	}

	_getLangMenuButtons(locale) {
		const buttons = [];
		let buttonsRow = [];
		const buttonsText = locale.settings.keyboard.langMenu;
		const callbackData = SettingsCommand.getLangMenuCallbackData();
		buttonsText.forEach((buttonText, index) => {
			if (index < buttonsText.length - 1) {
				buttonsRow.push({
					text: buttonText,
					callback_data: callbackData[index]
				});

				if (index > 0 && (index % 2) > 0) {
					buttons.push(buttonsRow);

					if (index + 1 < buttonsText.length) {
						buttonsRow = [];
					}
				}
			}
		});
		buttonsRow.push({
			text: buttonsText[buttonsText.length - 1],
			callback_data: callbackData[buttonsText.length - 1]
		});
		buttons.push(buttonsRow);
		return buttons;
	}

	_updateHighlightCorrectionsState(state) {
		const findQuery = { 
			chatId: this.getChatId() 
		};
		const setQuery = { 
			$set: { 
				highlightCorrections: JSON.parse(state) 
			}
		};
		return Settings.update(findQuery, setQuery).exec();
	}

	_updateMenuLanguage(lang) {
		const findQuery = { 
			chatId: this.getChatId() 
		};
		const setQuery = { 
			$set: { 
				menuLang: lang
			}
		};
		return Settings.update(findQuery, setQuery).exec();
	}

	_sendMessageIfUserHasNoRights(locale) {
		const self = this;
		return new Promise((resolve, reject) => {
			if (self.getChatId() < 0) {
				self.isUserGroupAdministrator()
				.then(isAdmin => {
					if (!isAdmin) {
						self.sendPlainMessage(locale.settings.noRightsText)
						.then(() => {
							resolve(true);
						});
					} else {
						resolve(false);
					}
				})
				.catch(reject);
			} else {
				resolve(false);
			}
		});
	}

	_showHighlightMenu() {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getSettings()
			.then(settings => {
				const locale = self.getLocale(settings.menuLang);
				self._sendMessageIfUserHasNoRights(locale)
				.then(messageSent => {
					if (!messageSent) {
						let text = locale.settings.text.highlightMenu;
						return self.sendMessageWithInlineButtons(true, text, self._getHighlightMenuButtons(locale));
					}
				});
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_showLangMenu() {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getSettings()
			.then(settings => {
				const locale = self.getLocale(settings.menuLang);
				self._sendMessageIfUserHasNoRights(locale)
				.then(messageSent => {
					if (!messageSent) {
						let text = locale.settings.text.langMenu;
						return self.sendMessageWithInlineButtons(true, text, self._getLangMenuButtons(locale));
					}
				});
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_showMainMenu(edit) {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getSettings()
			.then(settings => {
				const locale = self.getLocale(settings.menuLang);
				self._sendMessageIfUserHasNoRights(locale)
				.then(messageSent => {
					if (!messageSent) {
						const highlightState = locale.settings.dbText.highlightMenu[+settings.highlightCorrections];
						const menuLang = locale.settings.dbText.langMenu[settings.menuLang];
						let text = locale.settings.text.mainMenu;
						text = text.replace('{0}', highlightState);
						text = text.replace('{1}', menuLang);
						return self.sendMessageWithInlineButtons(edit, text, self._getMainMenuButtons(locale));
					}
				});
			})
			.then(resolve)
			.catch(reject);
		});
	}

	showSubMenu(data) {
		if (data === 'show_highlight_menu') {
			this.setCommandDesc('/settings:::highlight');
			return this._showHighlightMenu();
		} else if (data === 'show_lang_menu') {
			this.setCommandDesc('/settings:::lang');
			return this._showLangMenu();
		}
	}

	execHighlightMenuCommand(data) {
		if (data !== 'back_from_h') {
			const self = this;
			return new Promise((resolve, reject) => {
				self._updateHighlightCorrectionsState(data)
				.then(self._showMainMenu.bind(self, true))
				.then(resolve)
				.catch(reject);
			});
		} 

		return this._showMainMenu(true);
	}

	execLangMenuCommand(data) {
		if (data !== 'back_from_l') {
			const self = this;
			return new Promise((resolve, reject) => {
				self._updateMenuLanguage(data)
				.then(self._showMainMenu.bind(self, true))
				.then(resolve)
				.catch(reject);
			});
		} 

		return this._showMainMenu(true);
	}
	
	exec() {
		this.setCommandDesc('/settings');
		return this._showMainMenu(false);
	}
};	

module.exports = SettingsCommand;