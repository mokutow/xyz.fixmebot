'use strict';

/**
 * Module dependencies.
 */

const Promise = require('bluebird');
const mongoose = require('mongoose');
const Settings = mongoose.model('Settings');
const locale = require('./../locale/locale');
const TelegramApi = require('./../util/telegram-api');

/**
 * BaseCommand class.
 */

class BaseCommand {

	constructor(bot, message) {
		this.bot = bot;
		this.message = message;
		this.inline = false;
		this.commandDesc = null;
	}

	setInline(inline) {
		this.inline = inline;
	}

	isInline() {
		return this.inline;
	}

	setCommandDesc(commandDesc) {
		this.commandDesc = commandDesc;
	}

	getCommandDesc() {
		return this.commandDesc;
	}

	isGroupMessage() {
		return this.getChatId() < 0;
	}

	isUserGroupAdministrator() {
		//Telegram administrator status: “creator”, “administrator”
		const self = this;
		return new Promise((resolve, reject) => {
			const userId = self.message.from.id;
			TelegramApi.getChatAdministrators(self.getChatId())
			.then(admins => {
				for (let i = 0; i < admins.length; i++) {
					if (userId === admins[i].user.id) {
						return true;
					}
				}

				return false;
			})
			.then(isAdmin => {
				resolve(isAdmin);
			})
			.catch(reject);
		});
	}

	getChatId() {
		if ('message' in this.message) {
			return this.message.message.chat.id;
		}

		if ('chat' in this.message) {
			return this.message.chat.id;
		}

		return this.message.from.id;
	}

	getQueryId() {
		return this.message.id;
	}

	genResultQueryId() {
		const min = 0;
		const max = 1000;
		const random = Math.floor(Math.random() * (max - min)) + min;
		return '' + (new Date().getTime() + random);
	}

	getMessageId() {
		if ('message' in this.message) {
			return this.message.message.message_id;
		}

		return this.message.message_id;
	}

	getSettings() {
		const query = {
	        chatId: this.getChatId()
		};
		return Settings.findOne(query).exec();
	}

	getLocale(lang) {
		if (lang !== undefined) {
			return locale[lang];
		}

		return locale;
	}

	getBotLocale() {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getSettings()
			.then(settings => {
				const botLocale = self.getLocale(settings.menuLang); 
				resolve(botLocale);
			})
			.catch(reject);
		});
	}

	sendPlainMessage(text) {
		const params = {
			chat_id: this.getChatId(), 
			text: text,
			parse_mode: 'Markdown'
		};
		return this.bot.sendMessage(params);
	}

	sendInlineMessage(type, messageDetails) {
		const params = {
			inline_query_id: this.getQueryId(),
			results: [{
				id: this.genResultQueryId(),
			    type: type,
			    title: messageDetails.title,
			    description: messageDetails.description,
			    message_text: messageDetails.message_text
			}]
		};
		return this.bot.answerInlineQuery(params);
	}

	sendMessageWithInlineButtons(edit, text, buttons) {
		let params = {
			chat_id: this.getChatId(), 
			text: text,
			parse_mode: 'Markdown',
			reply_markup: JSON.stringify({
				inline_keyboard: buttons
			})
		};

		if (edit) {
			params.message_id = this.getMessageId();
			return this.bot.editMessageText(params);
		}

		return this.bot.sendMessage(params);
	}

	sendMessageWithKeyboard(text, keyboard) {
		const params = {
			chat_id: this.getChatId(), 
			text: text,
			parse_mode: 'Markdown',
			reply_markup: JSON.stringify({
				keyboard: keyboard,
				resize_keyboard: true
			})
		};
		return this.bot.sendMessage(params);
	}

	sendNotification(text, showAlert) {
		const params = {
		    callback_query_id: this.getQueryId(),
		    text: text,
		    show_alert: showAlert
		};
		return this.bot.answerCallbackQuery(params);
	}
};

module.exports = BaseCommand;