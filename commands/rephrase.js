'use strict';

/**
 * Module dependencies.
 */

const _ = require('lodash');
const Promise = require('bluebird');
const Requestify = require('requestify'); 
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Settings = mongoose.model('Settings');
const PendingCommand = require('./pending');

/**
 * RephraseCommand class.
 */

class RephraseCommand extends PendingCommand {

	static canExec(message) {
		return ('text' in message && 
				message.text.toLowerCase().startsWith('/re'));
	}

	static getCallbackData() {
		return [
			'/re'
		];
	}

	static getCommandNames() {
		const names = [ 
			'/re@' + process.env.BOT_NAME,
			'/re' 
		];
		return names;
	}

	_hasCommandNameOnly() {
		return ('text' in this.message && 
				_.includes(RephraseCommand.getCommandNames(), this.message.text.toLowerCase()));
	}

	_hasCommandNameAndText() {
		const cmdNames = RephraseCommand.getCommandNames();
		
		if ('text' in this.message) {
			const text = this.message.text.toLowerCase();

			for (let i = 0; i < cmdNames.length; i++) {
				const pattern = '\\' + cmdNames[i] + '[ ]{1}.+';
				const regExp = new RegExp(pattern, 'g');

				if (regExp.test(text)) {
					return true;
				}
			}
		}

		return false;
	}

	_hasTextOnly() {
		return ('text' in this.message && 
				!this.message.text.toLowerCase().startsWith('/re'));
	}

	_makeRequestUrl(text) {
		return process.env.GINGER_REPHRASE_ENDPOINT + '?s=' + encodeURI(text);
	}

	_rephraseText(text) {
		const self = this;
		return new Promise(function(resolve, reject) {
			self._rephraseEnglishText(text)
			.then(result => {
				self.getBotLocale()
				.then(locale => {
					if (!result.found) {
						return self.sendPlainMessage(locale.rephrase.noAltFoundText);
					} else {
						let resultMessage = locale.rephrase.altFoundText;
						result.variants.forEach((variant, index) => {
							resultMessage += '#' + (index + 1) + '.\t\t' + variant + '\n';
						});
						return self.sendPlainMessage(resultMessage);
					}
				})
				.then(resolve)
				.catch(reject);
			})
			.catch(reject);
		});
	}

	_rephraseEnglishText(text) {
		const self = this;
		return new Promise(function(resolve, reject) {
			const url = self._makeRequestUrl(text);
			Requestify.get(url)
			.then(response => {
			    const body = response.getBody();

			    if ('Sentences' in body) {
			    	let result = {
				    	found: false,
				    	variants: []
				    };
				    const variants = body.Sentences;

			    	if (variants.length > 0) {
			    		result.found = true;
			    		variants.forEach(variant => {
			    			result.variants.push(variant.Sentence);
			    		});
			    	}

			    	resolve(result);
			    } else {
			    	reject(new Error('rephrase.js: _rephraseEnglishText(): response body has no property "Sentences"'));
			    }
			})
			.catch(reject);
		});
	}

	_sendPendingMessage(isGroupRequest) {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getBotLocale()
			.then(locale => {
				let pendingText = '';

				if (isGroupRequest) {
					pendingText = locale.rephrase.groupPendingText;
				} else {
					pendingText = locale.rephrase.userPendingText;
				}

				return self.sendPlainMessage(pendingText);
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_execRequestWithCommandName() {
		const self = this;
		return new Promise((resolve, reject) => {
			if (self.isGroupMessage()) {
				self._sendPendingMessage(true)
				.then(resolve)
				.catch(reject);
			} else {
				self._updatePendingCommand('/re')
				.then(() => {
					return self._sendPendingMessage(false);
				})
				.then(resolve)
				.catch(reject);
			}
		});
	}

	_execCommand(textOnly) {
		const self = this;
		return new Promise((resolve, reject) => {
			let text = '';

			if (!textOnly) {
				const cmdNames = RephraseCommand.getCommandNames();

				for (let i = 0; i < cmdNames.length; i++) {
					const cmdName = cmdNames[i] + ' ';

					if (self.message.text.startsWith(cmdName)) {
						text = self.message.text.replace(cmdName, '');
						break;
					}
				}
			} else {
				text = self.message.text;
			}

			self._rephraseText(text)
			.then(resolve)
			.catch(reject);
		});
	}

	execPending() {
		this.setCommandDesc('/re:::pending:::exec:::button');
		const self = this;
		return new Promise((resolve, reject) => {
			self.getBotLocale()
			.then(locale => {
				return self.sendNotification(locale.rephrase.pendingNotifText, false);
			})
			.then(() => {
				return self._getPendingText();
			})
			.then(pendingText => {
				return self._rephraseText(pendingText);
			})
			.then(resolve)
			.catch(reject);
		});
	}

	exec() {
		const self = this;
		return new Promise((resolve, reject) => {
			if (self._hasCommandNameOnly()) {
				self.setCommandDesc('/re:::pending:::reg');
				self._execRequestWithCommandName()
				.then(resolve)
				.catch(reject);
			} 

			if (self._hasCommandNameAndText()) {
				self.setCommandDesc('/re:::+text');
				self._execCommand(false)
				.then(resolve)
				.catch(reject);
			}

			if (self._hasTextOnly()) {
				self.setCommandDesc('/re:::pending:::exec:::withoutbutton');
				self._execCommand(true)
				.then(() => {
					return self._updatePendingCommand(null);
				})
				.then(resolve)
				.catch(reject);
			}
		});
	}
};

module.exports = RephraseCommand;