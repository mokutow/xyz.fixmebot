'use strict';

/**
 * Module dependencies.
 */

const Promise = require('bluebird');
const mongoose = require('mongoose');
const Group = mongoose.model('ChatGroup');
const BaseCommand = require('./base.js');

/**
 * LeftChatCommand class.
 */

class LeftChatCommand extends BaseCommand {

	static canExec(message) {
		if ('left_chat_member' in message && 
			message.left_chat_member.username === process.env.BOT_NAME &&
			message.chat.type === 'group') {
			return true;
		}

		return false;
	}

	_updateBlockDate() {
		const findQuery = { 
			chatId: this.getChatId() 
		};
		const setQuery = { 
			$set: { 
				blockDate: Date.now() 
			}
		};
		return Group.update(findQuery, setQuery).exec();
	}

	exec() {
		this.setCommandDesc('removed_from_group_chat');
		return this._updateBlockDate();
	}
};

module.exports = LeftChatCommand;