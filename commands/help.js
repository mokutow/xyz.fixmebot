'use strict';

/**
 * Module dependencies.
 */

const Promise = require('bluebird');
const BaseCommand = require('./base.js');
const mongoose = require('mongoose');
const Settings = mongoose.model('Settings');

/**
 * HelpCommand class.
 */

class HelpCommand extends BaseCommand {

	static canExec(message) {
		if ('text' in message && message.text.toLowerCase().startsWith('/help')) {
			return true;
		}

		return false;
	}

	exec() {
		this.setCommandDesc('/help');
		const self = this;
		return new Promise((resolve, reject) => {
			self.setCommandDesc('/help');
			self.getBotLocale()
			.then(locale => {
				return self.sendPlainMessage(locale.help);
			})
			.then(resolve)
			.catch(reject);
		});
	}
};

module.exports = HelpCommand;