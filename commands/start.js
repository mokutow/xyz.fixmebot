'use strict';

/**
 * Module dependencies.
 */

const Promise = require('bluebird');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Group = mongoose.model('ChatGroup');
const Settings = mongoose.model('Settings');
const BaseCommand = require('./base.js');

/**
 * StartCommand class.
 */

class StartCommand extends BaseCommand {

	static isUserStartCommand(message) {
		return ('text' in message && 
				'chat' in message &&
				message.chat.id > 0 &&
				message.text.toLowerCase() === '/start');
	}

	static isGroupStartCommand(message) {
		return ('new_chat_member' in message &&
				message.new_chat_member.username === process.env.BOT_NAME &&
				message.chat.type === 'group');
	}

	static canExec(message) {
		return (StartCommand.isGroupStartCommand(message) ||
			    StartCommand.isUserStartCommand(message));
	}

	_isUserRegistered() {
		const query = {
        	chatId: this.getChatId()
      	};
		return User.findOne(query).exec();
	}

	_isGroupRegistered() {
		const query = {
        	chatId: this.getChatId()
      	};
		return Group.findOne(query).exec();
	}

	_createUser() {
		const user = {
			chatId: this.getChatId(),
			firstName: this.message.chat.first_name,
			lastName: this.message.chat.last_name,
			startDate: Date.now(),
			blockDate: null
		};
		return User.create(user);
	}

	_createGroup() {
		const group = {
			chatId: this.getChatId(),
			title: this.message.chat.title,
			startDate: Date.now(),
			blockDate: null
		};
		return Group.create(group);
	}

	_initSettings() {
		const settings = {
			chatId: this.getChatId(),
			menuLang: process.env.DEFAULT_LANG,
			highlightCorrections: true
		};
		return Settings.create(settings);
	}

	_registerUser() {
		const self = this;
		return new Promise((resolve, reject) => {
			self._isUserRegistered()
			.then(user => {
				if (user) {
					resolve();
				} else {
					self._createUser()
					.then(() => {
						return self._initSettings();
					})
					.then(resolve)
					.catch(reject);
				}
			})
			.catch(reject);
		});
	}

	_registerGroup() {
		const self = this;
		return new Promise((resolve, reject) => {
			self._isGroupRegistered()
			.then(group => {
				if (group) {
					resolve();
				} else {
					self._createGroup()
					.then(() => {
						return self._initSettings();
					})
					.then(resolve)
					.catch(reject);
				}
			})
			.catch(reject);
		});
	}

	_sendStartMessage() {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getBotLocale()
			.then(locale => {
				const startMessage = locale.start.headerText + locale.start.bodyText;
				return self.sendPlainMessage(startMessage);
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_execUser() {
		const self = this;
		return new Promise((resolve, reject) => {
			self._registerUser()
			.then(() => {
				return self._sendStartMessage();
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_execGroup() {
		const self = this;
		return new Promise((resolve, reject) => {
			self._registerGroup()
			.then(() => {
				return self._sendStartMessage();
			})
			.then(resolve)
			.catch(reject);
		});
	}

	exec() {
		if (StartCommand.isGroupStartCommand(this.message)) {
			this.setCommandDesc('/start:::group');
			return this._execGroup();
		} else {
			this.setCommandDesc('/start:::user');
			return this._execUser();
		}
	}
};

module.exports = StartCommand;