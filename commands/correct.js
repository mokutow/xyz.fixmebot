'use strict';

/**
 * Module dependencies.
 */

const _ = require('lodash');
const Promise = require('bluebird');
const Gingerbread = require('gingerbread');
const Diff = require('diff');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Settings = mongoose.model('Settings');
const PendingCommand = require('./pending.js');

/**
 * Constants.
 */

const MIN_TEXT_LENGTH = 3;

/**
 * CorrectCommand class.
 */

class CorrectCommand extends PendingCommand {

	static canExec(message) {
		return ('text' in message && 
				message.text.toLowerCase().startsWith('/co'));
	}

	static getCallbackData() {
		return [
			'/co'
		];
	}

	static getCommandNames() {
		const names = [ 
			'/co@' + process.env.BOT_NAME,
			'/co' 
		];
		return names;
	}

	_hasCommandNameOnly() {
		return ('text' in this.message && 
				_.includes(CorrectCommand.getCommandNames(), this.message.text.toLowerCase()));
	}

	_hasCommandNameAndText() {
		const cmdNames = CorrectCommand.getCommandNames();
		
		if ('text' in this.message) {
			const text = this.message.text.toLowerCase();

			for (let i = 0; i < cmdNames.length; i++) {
				const pattern = '\\' + cmdNames[i] + '[ ]{1}.+';
				const regExp = new RegExp(pattern, 'g');

				if (regExp.test(text)) {
					return true;
				}
			}
		}

		return false;
	}

	_hasTextOnly() {
		return ('text' in this.message && 
				!this.message.text.toLowerCase().startsWith('/co'));
	}

	_isHighlightCorrectionsEnabled() {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getSettings()
			.then(settings => {
				resolve(settings.highlightCorrections);
			})
			.catch(reject);
		});
	}

	_sendPendingMessage(isGroupRequest) {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getBotLocale()
			.then(locale => {
				let pendingText = '';

				if (isGroupRequest) {
					pendingText = locale.correct.groupPendingText;
				} else {
					pendingText = locale.correct.userPendingText;
				}

				return self.sendPlainMessage(pendingText);
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_sendTooShortMessage(text) {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getBotLocale()
			.then(locale => {
				if (!self.isInline()) {
					return self.sendPlainMessage(locale.correct.tooShortText.plain);
				} else {
					const messageDetails = {
						title: locale.correct.tooShortText.inline.title,
					    description: locale.correct.tooShortText.inline.description,
					    message_text: text
					};
					return self.sendInlineMessage('article', messageDetails);
				}
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_sendNoErrMessage(origText) {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getBotLocale()
			.then(locale => {
				if (!self.isInline()) {
					return self.sendPlainMessage(locale.correct.noErrText);
				} else {
					const messageDetails = {
						title: locale.correct.noErrText,
					    description: origText,
					    message_text: origText
					};
					return self.sendInlineMessage('article', messageDetails);
				}
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_sendCorrectedMessage(corrText) {
		const self = this;
		return new Promise((resolve, reject) => {
			self.getBotLocale()
			.then(locale => {
				if (!self.isInline()) {
					return self.sendPlainMessage(corrText);
				} else {
					const messageDetails = {
						title: locale.correct.correctedText,
					    description: corrText,
					    message_text: corrText
					};
					return self.sendInlineMessage('article', messageDetails);
				}
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_correctEnglishText(text) {
		return new Promise(function(resolve, reject) {
			Gingerbread(text, function (err, origText, corrText, corrArr) {
				if (err) {
					reject(err);
				} else {
					const result = [ 
						corrText, 
						corrArr 
					];
					resolve(result);
				}
			});
		});
	}

	_highlightTextDiff(origText, corrText) {
		const self = this;
		return new Promise((resolve, reject) => {
			let resultText = '';
			const textDiff = Diff.diffWords(origText, corrText);
			textDiff.forEach(textDiffPart => {
				if ('added' in textDiffPart && 'removed' in textDiffPart) {
					if (textDiffPart.added) {
						resultText += '*' + textDiffPart.value + '*';
					}
				} else {
					resultText += textDiffPart.value;
				}
			});
			resolve(resultText);
		});
	}

	_correctText(text) {
		const self = this;
		return new Promise((resolve, reject) => {
			self._correctEnglishText(text)
			.then(result => {
				let corrText = result[0];
				const corrArr = result[1];

				if (corrArr.length === 0 || (corrText.toLowerCase() === text.toLowerCase())) {
					return self._sendNoErrMessage(text);
				} else {
					if (!self.isInline()) {
						self._isHighlightCorrectionsEnabled()
						.then(enabled => {
							if (enabled) {
								self._highlightTextDiff(text, corrText)
								.then(highlightedText => {
									return self._sendCorrectedMessage(highlightedText);
								})
								.then(resolve)
								.catch(reject);
							} else {
								return self._sendCorrectedMessage(corrText);
							}
						})
						.then(resolve)
						.catch(reject);
					} else {
						return self._sendCorrectedMessage(corrText);
					}
				}
			})
			.then(resolve)
			.catch(reject);
		});
	}

	_execRequestWithCommandName() {
		const self = this;
		return new Promise((resolve, reject) => {
			if (self.isGroupMessage()) {
				self._sendPendingMessage(true)
				.then(resolve)
				.catch(reject);
			} else {
				self._updatePendingCommand('/co')
				.then(() => {
					return self._sendPendingMessage(false);
				})
				.then(resolve)
				.catch(reject);
			}
		});
	}

	_execCommand(textOnly) {
		const self = this;
		return new Promise((resolve, reject) => {
			let text = '';

			if (self.isInline()) {
				text = self.message.query;
			} else {
				if (!textOnly) {
					const cmdNames = CorrectCommand.getCommandNames();

					for (let i = 0; i < cmdNames.length; i++) {
						const cmdName = cmdNames[i] + ' ';

						if (self.message.text.startsWith(cmdName)) {
							text = self.message.text.replace(cmdName, '');
							break;
						}
					}
				} else {
					text = self.message.text;
				}
				
			}

			if (text.length === 0 && self.isInline()) {
				resolve();
			} else if (text.length < MIN_TEXT_LENGTH) {
				self._sendTooShortMessage(text)
				.then(resolve)
				.catch(reject);
			} else {
				self._correctText(text)
				.then(resolve)
				.catch(reject);
			}
		});
	}

	execPending() {
		this.setCommandDesc('/co:::pending:::exec:::button'); 
		const self = this;
		return new Promise((resolve, reject) => {
			self.getBotLocale()
			.then(locale => {
				return self.sendNotification(locale.correct.pendingNotifText, false);
			})
			.then(() => {
				return self._getPendingText();
			})
			.then(pendingText => {
				return self._correctText(pendingText);
			})
			.then(resolve)
			.catch(reject);
		});
	}

	execInline() {
		this.setCommandDesc('/co:::inline');
		const self = this;
		return new Promise((resolve, reject) => {
			self.setInline(true);
			self._execCommand()
			.then(resolve)
			.catch(reject);
		});
	}

	exec() {
		const self = this;
		return new Promise((resolve, reject) => {
			if (self._hasCommandNameOnly()) {
				self.setCommandDesc('/co:::pending:::reg');
				self._execRequestWithCommandName()
				.then(resolve)
				.catch(reject);
			} 

			if (self._hasCommandNameAndText()) {
				self.setCommandDesc('/co:::+text');
				self._execCommand(false)
				.then(resolve)
				.catch(reject);
			}

			if (self._hasTextOnly()) {
				self.setCommandDesc('/co:::pending:::exec:::withoutbutton');
				self._execCommand(true)
				.then(() => {
					return self._updatePendingCommand(null);
				})
				.then(resolve)
				.catch(reject);
			}
		});
	}
};

module.exports = CorrectCommand;