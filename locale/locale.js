'use strict';

module.exports = {
	en: {
		start: {
			headerText: 'Hello! \uD83D\uDE00\n\n',
			bodyText: 'With @fixmebot you won’t need to waste time trying to find the correct way to write a sentence in \uD83C\uDDEC\uD83C\uDDE7 *English*, or have to pass your texts to someone else for review. Checking grammar has never been easier.\n\n' +
		    		  'Type /help to see available commands.'
		},
		help: '*Command list:* \n\n' +
			  '1. Correct mistakes:\n' +
			  '\t\t\t\t/co\n' +
			  '2. Get suggestions for rephrasing the original sentence:\n' + 
	   		  '\t\t\t\t/re\n' +
	   		  '3. Bot settings:\n' + 
	   		  '\t\t\t\t/settings\n' +
	   		  '4. Help:\n' + 
	   		  '\t\t\t\t/help\n\n' +
			  '*Inline mode:*\n\n' +
			  'Type @fixmebot in the message field in any chat, then type some text. The bot will check and correct mistakes in your message. \nFor example: `@fixmebot Hellow world!`\n\n' +
			  '*Where can bot be used?*\n' +
			  '1. Ordinary 1-1 chats\n' +
			  '2. Group chats\n\n' +
			  '*Supported language:* \uD83C\uDDEC\uD83C\uDDE7 English\n' +
			  'More languages will be available soon. Stay tuned!\n\n' +
			  '*Support chat:*\n' +
			  'https://telegram.me/fixmebotsupport\n\n', //+
			  /*'*Website:*\n' +
			  'https://fixmebot.xyz/',*/
		settings: {
			text: {
				mainMenu: 'Current settings:\n\n' +
			          	  '🔸 Highlight mode: *{0}*\n' +
			         	  '🔸 Menu language: *{1}*\n\n',
				highlightMenu: 'Highlight corrections?',
				langMenu: 'Select menu language:'
			},
			dbText: {
				highlightMenu: [ 
					'off', 
					'on' 
				],
				langMenu: {
					en: 'English',
					ru: 'Russian'
				}
			},
			keyboard: {
				mainMenu: [ 
					'Highlight mode', 
					'Menu language'
				],
				highlightMenu: [ 
					'Yes', 
					'No',
					'\uD83D\uDD19 Back' 
				],
				langMenu: [ 
					'\uD83C\uDDEC\uD83C\uDDE7 English', 
					'\uD83C\uDDF7\uD83C\uDDFA Russian',
					'\uD83D\uDD19 Back'  
				]
			},
			noRightsText: 'You have no rights. Only group administrators can change bot settings.'
		},
		correct: {
			pendingNotifText: 'Processing /co command...',
			userPendingText: 'Now send me the text to correct mistakes',
			groupPendingText: 'Type `/co@fixmebot {message}`, where `{message}`  -  text to correct.\n\n' + 
			                  'For example: `/co@fixmebot Hellow world!`',
			tooShortText: {
				plain: 'The message is too short. Minimum message length is 3 characters.',
				inline: {
					title: 'The message is too short',
			    	description: 'Minimum message length is 3 characters'
				}
			},
			noErrText: 'The message is written with no errors',
			correctedText: 'Mistakes corrected:'
		},
		rephrase: {
			pendingNotifText: 'Processing /re command...',
			userPendingText: 'Now send me the text to rephrase',
			groupPendingText: 'Type `/re@fixmebot {message}`, where `{message}`  -  text to rephrase.\n\n' + 
			                  'For example: `/re@fixmebot Nice car`',
			altFoundText: '*Suggestions:*\n\n',
			noAltFoundText: 'Suggestions not found'
		},
		nonCommand: {
			messageText: 'I received your message. What can I do for you?',
			buttonsText: {
				correct: 'Correct mistakes',
				rephrase: 'Rephrase'
			}
		}
	},
	ru: {
		start: {
			headerText: 'Hello! \uD83D\uDE00\n\n',
			bodyText: 'With @fixmebot you won’t need to waste time trying to find the correct way to write a sentence in \uD83C\uDDEC\uD83C\uDDE7 *English*, or have to pass your texts to someone else for review. Checking grammar has never been easier.\n\n' +
		    		  'Type /help to see available commands.'
		},
		help: '*Список команд:* \n\n' +
			  '1. Исправить ошибки:\n' +
			  '\t\t\t\t/co\n' +
			  '2. Перефразировать предложение:\n' + 
	   		  '\t\t\t\t/re\n' +
	   		  '3. Настройки бота:\n' + 
	   		  '\t\t\t\t/settings\n' +
	   		  '4. Помощь:\n' + 
	   		  '\t\t\t\t/help\n\n' +
			  '*Inline-режим:*\n\n' +
			  'Наберите @fixmebot в поле ввода любого чата, затем допишите текст сообщения. Бот проверит и исправит ошибки в вашем сообщении. \nК примеру: `@fixmebot Hellow world!`\n\n' +
			  '*В какие чаты можно добавлять бота?*\n' +
			  '1. Обычные чаты 1-1\n' +
			  '2. Групповые чаты\n\n' +
			  '*Поддерживаемые языки:* \uD83C\uDDEC\uD83C\uDDE7 Английский\n' +
			  'Новые языки скоро будут добавлены. Оставайтесь с нами!\n\n' +
			  '*Чат службы поддержки:*\n' +
			  'https://telegram.me/fixmebotsupport\n\n', //+
			  /*'*Веб-сайт:*\n' +
			  'https://fixmebot.xyz/',*/
		settings: {
			text: {
				mainMenu: 'Текущие настройки:\n\n' +
			          	  '🔸 Подсветка исправлений: *{0}*\n' +
			          	  '🔸 Язык меню: *{1}*\n\n',
			    highlightMenu: 'Подсвечивать исправления?',
				langMenu: 'Выберите язык меню:'
			},
			dbText: {
				highlightMenu: [ 
					'выкл.', 
					'вкл.' 
				],
				langMenu: {
					en: 'Английский',
					ru: 'Русский'
				}
			},
			keyboard: {
				mainMenu: [ 
					'Подсветка исправлений', 
					'Язык меню'
				],
				highlightMenu: [ 
					'Вкл.', 
					'Выкл.',
					'\uD83D\uDD19 Назад'  
				],
				langMenu: [ 
					'\uD83C\uDDEC\uD83C\uDDE7 Английский', 
					'\uD83C\uDDF7\uD83C\uDDFA Русский',
					'\uD83D\uDD19 Назад'  
				]
			},
			noRightsText: 'У вас нет прав. Только администраторы групп могут менять настройки бота.'
		},
		correct: {
			pendingNotifText: 'Выполняется команда /co...',
			userPendingText: 'Теперь отправьте мне текст и я исправлю в нем ошибки',
			groupPendingText: 'Наберите `/co@fixmebot {message}`, где `{message}`  -  текст для проверки.\n\n' +
							  'К примеру: `/co@fixmebot Hellow world!`',
			tooShortText: {
				plain: 'Текст сообщения слишком короткий. Минимальная длина текста - 3 символа.',
				inline: {
					title: 'Текст сообщения слишком короткий',
			    	description: 'Минимальная длина текста 3 символа'
				}
			},
			noErrText: 'Сообщение написано без ошибок',
			correctedText: 'Ошибки исправлены:'
		},
		rephrase: {
			pendingNotifText: 'Выполняется команда /re...',
			userPendingText: 'Теперь отправьте мне текст для перефразирования',
			groupPendingText: 'Наберите `/re@fixmebot {message}`, где `{message}`  -  текст для перефразирования.\n\n' +
							  'К примеру: `/re@fixmebot Nice car`',
			altFoundText: '*Найдены варианты:*\n\n',
			noAltFoundText: 'Варианты не найдены'
		},
		nonCommand: {
			messageText: 'Я получил ваше сообщение. Чем могу быть полезен?',
			buttonsText: {
				correct: 'Найди и исправь ошибки',
				rephrase: 'Перефразируй'
			}
		}
	}
};