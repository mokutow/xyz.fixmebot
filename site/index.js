const fs = require('fs');
const https = require('https');
const http = require('http');
const express = require('express');
const app = express();

const options = {
	ca: fs.readFileSync('/root/fixmebot_ssl/comodo/fixmebot_xyz.ca-bundle'),
	key: fs.readFileSync('/root/fixmebot_ssl/fixmebot.xyz.key'),
	cert: fs.readFileSync('/root/fixmebot_ssl/comodo/fixmebot_xyz.crt')
};

app.use(express.static('public'));

http.createServer(app).listen(80);
https.createServer(options, app).listen(443);