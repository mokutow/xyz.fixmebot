'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');

/**
 * Group Model Definition.
 * @type {Schema}
 */

const GroupSchema = new mongoose.Schema({
	chatId: Number,
	title: String,
	startDate: Date,
	blockDate: Date
});

mongoose.model('ChatGroup', GroupSchema);
module.exports = exports = GroupSchema;