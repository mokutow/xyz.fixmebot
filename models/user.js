'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');

/**
 * User Model Definition.
 * @type {Schema}
 */

const UserSchema = new mongoose.Schema({
	chatId: Number,
	firstName: String,
	lastName: String,
	startDate: Date,
	blockDate: Date,
	pendingCommand: String,
	textToProcess: String
});

mongoose.model('User', UserSchema);
module.exports = exports = UserSchema;