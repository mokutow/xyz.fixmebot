'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');

/**
 * Error Model Definition.
 * @type {Schema}
 */

const ErrorSchema = new mongoose.Schema({
	message: String,
	stack: String,
	timestamp: Date
});

mongoose.model('Error', ErrorSchema);
module.exports = exports = ErrorSchema;