'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');

/**
 * Settings Model Definition.
 * @type {Schema}
 */

const SettingsSchema = new mongoose.Schema({
	chatId: Number,
	menuLang: String,
	highlightCorrections: Boolean,
});

mongoose.model('Settings', SettingsSchema);
module.exports = exports = SettingsSchema;