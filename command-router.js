'use strict';

/**
 * Module dependencies.
 */

const _ = require('lodash');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const StartCommand = require('./commands/start');
const CorrectCommand = require('./commands/correct');
const RephraseCommand = require('./commands/rephrase');
const SettingsCommand = require('./commands/settings');
const LeftChatCommand = require('./commands/left-chat');
const TextCommand = require('./commands/text');
const HelpCommand = require('./commands/help');

/**
 * CommandRouter class.
 */

class CommandRouter {

	constructor(bot) {
		this.bot = bot;
	}

	_getPendingCommand(message) {
		return new Promise((resolve, reject) => {
			let chatId = undefined;

			if ('chat' in message) {
				chatId = message.chat.id;
			}

			if (chatId === undefined || chatId < 0) {
				resolve(null);
			}

			User.findOne({
			    chatId: chatId
			})
			.exec()
			.then(user => {
				resolve(user.pendingCommand);
			})
			.catch(err => {
				resolve(null);
			});
		});
	}

	_onRouteCompleted(resolve, command) {
		resolve([
			command.getChatId(), 
			command.getCommandDesc()
		]);
	}

	routeMessage(message) {
		//console.log(message);
		const commands = [
			StartCommand, 
			SettingsCommand, 
			HelpCommand, 
			CorrectCommand, 
			RephraseCommand, 
			LeftChatCommand
		];

		const self = this;
		return new Promise((resolve, reject) => {
			let command = null;

    		for (const cmd of commands) {
     			if (cmd.canExec(message)) {
       				command = new cmd(self.bot, message);
       				break;
       			}
     		}

     		if (command) {
				command.exec()
				.then(self._onRouteCompleted.bind(self, resolve, command))
				.catch(reject);
			} else {
				self._getPendingCommand(message)
				.then(pending => {
					if (pending === '/co') {
						command = new CorrectCommand(self.bot, message);
					} else if (pending === '/re') {
						command = new RephraseCommand(self.bot, message);
					} else {
						command = new TextCommand(self.bot, message);
					}

					command.exec()
					.then(self._onRouteCompleted.bind(self, resolve, command))
					.catch(reject);
				});
			}
     	});
	}

	routeInlineCallbackQuery(query) {
		//console.log(query);
		const self = this;
		return new Promise((resolve, reject) => {
			let command = null;
			const data = query.data;

			if (_.includes(SettingsCommand.getMainMenuCallbackData(), data))  {
				command = new SettingsCommand(self.bot, query);
				command.showSubMenu(data)
				.then(self._onRouteCompleted.bind(self, resolve, command))
				.catch(reject);
			} 

			if (_.includes(SettingsCommand.getHighlightMenuCallbackData(), data)) {
				command = new SettingsCommand(self.bot, query);
				command.execHighlightMenuCommand(data)
				.then(self._onRouteCompleted.bind(self, resolve, command))
				.catch(reject);
			} 

			if (_.includes(SettingsCommand.getLangMenuCallbackData(), data)) {
				command = new SettingsCommand(self.bot, query);
				command.execLangMenuCommand(data)
				.then(self._onRouteCompleted.bind(self, resolve, command))
				.catch(reject);
			}

			if (_.includes(CorrectCommand.getCallbackData(), data)) {
				command = new CorrectCommand(self.bot, query);
				command.execPending()
				.then(self._onRouteCompleted.bind(self, resolve, command))
				.catch(reject);
			}

			if (_.includes(RephraseCommand.getCallbackData(), data)) {
				command = new RephraseCommand(self.bot, query);
				command.execPending()
				.then(self._onRouteCompleted.bind(self, resolve, command))
				.catch(reject);
			}
		});
	}

	routeInlineQuery(query) {
		//console.log(query);
		const self = this;
		return new Promise((resolve, reject) => {
			const command = new CorrectCommand(self.bot, query);
			command.execInline()
			.then(self._onRouteCompleted.bind(self, resolve, command))
			.catch(reject);
		});
	}
};

module.exports = CommandRouter;